package co.mailtarget.coba.controller

import io.vertx.ext.web.Router

interface IController{
  fun main()
  fun checkName()
  fun listFruit()
  fun sendName()
}
