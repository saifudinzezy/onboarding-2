package co.mailtarget.coba.controller

import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router


class Controller(router: Router) : IController {
  private var router: Router

  init {
    this.router = router
  }

  //main
  override fun main() {
    router.route("/").handler { routingContext ->
      val response: HttpServerResponse = routingContext.response()
      response
        .putHeader("content-type", "application/json; charset=utf-8")
        .end(
          "{\n" +
            "    \"fruit\": \"Apple\",\n" +
            "    \"size\": \"Large\",\n" +
            "    \"color\": \"Red\"\n" +
            "}"
        )
    }
  }

  override fun checkName() {
    router.get("/checkName").handler { routingContext ->
      val response: HttpServerResponse = routingContext.response()
      //get params name
      val name: String? = routingContext.request().getParam("name") ?: "udin"

      response
        .putHeader("content-type", "application/json; charset=utf-8")
        .end(
          "{\n" +
            "    \"name\": \"${name?.capitalize()}\"\n" +
            "}"
        )
    }
  }

  //liat
  override fun listFruit() {
    val fruits = arrayListOf("Apple", "Manggo")
    var fruit = "{\n "
    for (i in fruits) {
      fruit += " \"fruit\" : \"$i\",\n"
    }
    fruit += " }"

    router.get("/listFruit").handler { routingContext ->
      val response: HttpServerResponse = routingContext.response()
      response
        .putHeader("content-type", "application/json; charset=utf-8")
        .end(
          fruit
        )
    }
  }

  override fun sendName() {
    router.route(HttpMethod.POST, "/sendName/").handler { routingContext ->
      val response: HttpServerResponse = routingContext.response()
      // the POSTed content is available in context.getBodyAsJson()
      val body: JsonObject = routingContext.getBodyAsJson()

      // a JsonObject wraps a map and it exposes type-aware getters
      val postedText = body.getString("text")

      response
        //.putHeader("content-type", "application/json; charset=utf-8")
        .end(
          "{\n" +
            "    \"name\": \"${postedText}\"\n" +
            "}"
        )
    }
  }
}
