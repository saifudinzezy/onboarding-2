package co.mailtarget.coba.verticle
import co.mailtarget.coba.controller.Controller
import io.vertx.core.AbstractVerticle
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.http.HttpServer
import io.vertx.ext.web.Router


class MyFirstVerticle : AbstractVerticle() {
  override fun start(fut: Future<Void>) {
    // Create a router object.
    val router: Router = Router.router(vertx)
    val controller = Controller(router)

    // Bind "/" to our hello message - so we are still compatible.
    controller.main()

    //list-fruit
    controller.listFruit()

    //checkName
    controller.checkName()

    //postName
    controller.sendName()

    // Create the HTTP server and pass the "accept" method to the request handler.
    vertx
      .createHttpServer()
      .requestHandler(router::accept)
      .listen( // Retrieve the port from the configuration,
        // default to 8080.
        config().getInteger("http.port", 8080)
      ) { result: AsyncResult<HttpServer?> ->
        if (result.succeeded()) {
          fut.complete()
        } else {
          fut.fail(result.cause())
        }
      }
  }
}
