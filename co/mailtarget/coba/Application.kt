package co.mailtarget.coba

import co.mailtarget.coba.verticle.MyFirstVerticle
import io.vertx.core.Vertx

class Application {
  companion object{
    @JvmStatic
    fun main(args: Array<String>) {
      var myFirstVerticle = MyFirstVerticle()
      Vertx.vertx().deployVerticle(
        myFirstVerticle
      )
    }
  }
}
